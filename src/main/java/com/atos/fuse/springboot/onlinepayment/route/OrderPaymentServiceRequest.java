package com.atos.fuse.springboot.onlinepayment.route;

import java.util.List;

import com.atos.fuse.springboot.onlinepayment.datamodel.Order;

public class OrderPaymentServiceRequest {

	private String customerId;
	private List<Order> orderList;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public List<Order> getOrderList() {
		return orderList;
	}
	public void setOrderList(List<Order> orderList) {
		this.orderList = orderList;
	}
	
	
}
