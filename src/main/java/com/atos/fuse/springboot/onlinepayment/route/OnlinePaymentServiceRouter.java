package com.atos.fuse.springboot.onlinepayment.route;

import javax.jms.ConnectionFactory;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.LoggingLevel;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.stereotype.Component;

import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CreditCardInfo;
import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CreditCardValidatorServiceRequest;
import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CustomerInfo;

@Component
public class OnlinePaymentServiceRouter extends RouteBuilder {

	int tracker = 0;
	
	@Override
	public void configure() throws Exception {
		restConfiguration().component("servlet").bindingMode(RestBindingMode.json);

		rest().get("/onlineorderpayment").type(OrderPaymentServiceRequest.class)
				.outType(OrderPaymentServiceResponse.class).to("direct:onlineorderpayment");
		tracker++;
		ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
	            "vm://embedded?broker.persistent=false,useShutdownHook=false");
	    getContext().addComponent("activemq",
	            JmsComponent.jmsComponentAutoAcknowledge(connectionFactory));
		
		from("direct:onlineorderpayment").log(LoggingLevel.INFO, "Order Payment Service is being called")
		.process(new Processor() {

					@Override
					public void process(Exchange exchange) throws Exception {
						System.out.println("Customer ID===>" + ( (OrderPaymentServiceRequest)exchange.getIn().getBody( )).getCustomerId());
						//exchange.getContext().getTypeConverterRegistry().addTypeConverter(CreditCardValidatorServiceRequest.class, String.class, new MyOrderTypeConverter());
						/*System.out.println("exchange===>" + exchange.getCreated());
						OrderPaymentServiceRequest orderRequest = (OrderPaymentServiceRequest) exchange.getIn()
								.getBody();
						System.out.println("orderRequest getCustomerId ===>" + orderRequest.getCustomerId());
						OrderPaymentServiceResponse response = new OrderPaymentServiceResponse();
						response.setPaymentId("12345");*/
						//exchange.getIn().setBody(response);
						CreditCardValidatorServiceRequest creditcardreq = new CreditCardValidatorServiceRequest();
						creditcardreq.setRequestId("12345");
						CreditCardInfo cardInfo = new CreditCardInfo();
						cardInfo.setCardNo(123243l);
						creditcardreq.setCardInfo(cardInfo);
						CustomerInfo customerInfo = new CustomerInfo();
						creditcardreq.setCustomerInfo(customerInfo);
						exchange.getIn().setBody("{  \"requestId\": \"1111\", \"cardInfo\": null, \"customerInfo\": null }");
						
						/*ObjectMapper mapper = new ObjectMapper();
						mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
						mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
						mapper.setVisibility(PropertyAccessor.GETTER, Visibility.ANY);
						mapper.setVisibility(PropertyAccessor.SETTER, Visibility.ANY);
						//Object to JSON in String
						String jsonInString = mapper.writeValueAsString(creditcardreq);
						exchange.getIn().setBody(jsonInString);*/
						System.out.println("++++++++++++++++++++++++");
						tracker++;
					}
				}).to("direct:step2");
		
		
		
		from("direct:step2").setHeader(Exchange.HTTP_METHOD, constant("POST")).setHeader(Exchange.HTTP_URI, constant("http://localhost:8081/creditcardservice/validate?throwExceptionOnFailure=false&bridgeEndpoint=true") )
	    .setHeader(Exchange.CONTENT_TYPE, constant("application/json")).to("http://customer-payment.springboot-payment-service-project.svc:8080/creditcardservice/validate?bridgeEndpoint=true").convertBodyTo(String.class).process(new Processor() {

			@Override
			public void process(Exchange exchange) throws Exception {
				
				System.out.println("Inside direct:getCustomerPersonalInfoRoute===>"+exchange.getIn().getHeader("customerDetailsRoutingSlipHeader"));
				exchange.getIn().setHeader("customerDetailsRoutingSlipHeader", "customerPersonalInfoRoutingSlipHeader");
				tracker++;
			}
		}).dynamicRouter(method(DynamicRouterBean.class, "route"));
		
		
		
		
		from("direct:getCustomerPersonalInfoRoute").process(new Processor() {
			public void process(Exchange exchange) {
				System.out.println("Inside direct:getCustomerPersonalInfoRoute===>"+exchange.getIn().getHeader("customerDetailsRoutingSlipHeader"));
				exchange.getIn().setHeader("customerDetailsRoutingSlipHeader", "customerBillingAddrRouteSlipHeader");
				tracker++;
			}
		});

		from("direct:getCustomerBillingAddrRoute").process(new Processor() {
			public void process(Exchange exchange) {
				System.out.println("Inside direct:getCustomerBillingAddrRoute====>"+exchange.getIn().getHeader("customerDetailsRoutingSlipHeader"));
				exchange.getIn().setHeader("customerDetailsRoutingSlipHeader", "sustomerShippingAddrRouteSlipHeader");
				tracker++;
			}
		});

		from("direct:getCustomerShippingAddrRoute").process(new Processor() {
			public void process(Exchange exchange) {
				System.out.println("Inside direct:getCustomerShippingAddrRoute=====>"+exchange.getIn().getHeader("customerDetailsRoutingSlipHeader"));
				System.out.println("Tracker======>"+tracker);
			}
		}).to(ExchangePattern.InOnly, "activemq:queue:order-queue").to("direct:receiveMessageFromOrderQ");
		
		from("direct:receiveMessageFromOrderQ").end().log("Before receiving message>>>>>>").process(exchange -> {
            String convertedMessage = exchange.getMessage().getBody() + " Payment received by the queue";
            exchange.getMessage().setBody(convertedMessage);
        }).end();
		
	}
}

