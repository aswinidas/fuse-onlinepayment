package com.atos.fuse.springboot.onlinepayment.route;

public class OrderPaymentServiceResponse {

	private String paymentId;

	public String getPaymentId() {
		return paymentId;
	}

	public void setPaymentId(String paymentId) {
		this.paymentId = paymentId;
	}
	
	
}
