package com.atos.fuse.springboot.onlinepayment.route;

import org.apache.camel.Exchange;
import org.apache.camel.NoTypeConversionAvailableException;
import org.apache.camel.TypeConversionException;
import org.apache.camel.TypeConverter;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class MyOrderTypeConverter implements TypeConverter {

	@Override
	public boolean allowNull() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public <T> T convertTo(Class<T> type, Object value) throws TypeConversionException {
		ObjectMapper mapper = new ObjectMapper();
		//Object to JSON in String
		try {
			String jsonInString = mapper.writeValueAsString(value);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public <T> T convertTo(Class<T> type, Exchange exchange, Object value) throws TypeConversionException {
		// TODO Auto-generated method stub
		return convertTo(type,value);
	}

	@Override
	public <T> T mandatoryConvertTo(Class<T> type, Object value)
			throws TypeConversionException, NoTypeConversionAvailableException {
		// TODO Auto-generated method stub
		return convertTo(type,value);
	}

	@Override
	public <T> T mandatoryConvertTo(Class<T> type, Exchange exchange, Object value)
			throws TypeConversionException, NoTypeConversionAvailableException {
		// TODO Auto-generated method stub
		return convertTo(type,value);
	}

	@Override
	public <T> T tryConvertTo(Class<T> type, Object value) {
		// TODO Auto-generated method stub
		return convertTo(type,value);
	}

	@Override
	public <T> T tryConvertTo(Class<T> type, Exchange exchange, Object value) {
		// TODO Auto-generated method stub
		return convertTo(type,value);
	}

}
