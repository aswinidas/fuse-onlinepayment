package com.atos.fuse.springboot.onlinepayment.route;

import org.apache.camel.Exchange;
import org.apache.camel.Header;

public class DynamicRouterBean {

	public String route(String body, @Header(Exchange.SLIP_ENDPOINT) String previousRoute) {
		if (previousRoute == null) {
			System.out.println("Inside 1111111");
			return "direct:getCustomerPersonalInfoRoute";
			// check the body content and decide route
		} else if (previousRoute.equalsIgnoreCase("direct://getCustomerPersonalInfoRoute")) {
			System.out.println("Inside 2222222");
			return "direct:getCustomerBillingAddrRoute";
			// check the body content and decide route
		} else if (previousRoute.equalsIgnoreCase("direct://getCustomerBillingAddrRoute")) {
			System.out.println("Inside 333333333333");
			return "direct:getCustomerShippingAddrRoute";
		} else {
			return null;
		}
	}
}
