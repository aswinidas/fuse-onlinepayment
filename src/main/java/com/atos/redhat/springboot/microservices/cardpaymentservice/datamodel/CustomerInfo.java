package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.ANY,setterVisibility=JsonAutoDetect.Visibility.ANY)
public class CustomerInfo  implements Serializable  {

	private String customerId;
	private String firstName;
	private String lastName;
	private String customerType;
	
	private CreditCardInfo cardDetails;
	private Address billingAddress;
	private Address ShippingAddress;
	
	public CreditCardInfo getCardDetails() {
		return cardDetails;
	}
	public void setCardDetails(CreditCardInfo cardDetails) {
		this.cardDetails = cardDetails;
	}
	public Address getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}
	public Address getShippingAddress() {
		return ShippingAddress;
	}
	public void setShippingAddress(Address shippingAddress) {
		ShippingAddress = shippingAddress;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	
	
}
