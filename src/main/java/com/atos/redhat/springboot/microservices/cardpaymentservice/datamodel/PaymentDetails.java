package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.ANY,setterVisibility=JsonAutoDetect.Visibility.ANY)
public class PaymentDetails implements Serializable  {

	private String paymentId;
	private CustomerInfo customerInfo;
	private CreditCardInfo creditCardInfo;
}
