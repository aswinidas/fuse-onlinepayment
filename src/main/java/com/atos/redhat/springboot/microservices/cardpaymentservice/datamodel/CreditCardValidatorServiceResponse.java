package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

import java.io.Serializable;

import com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel.CreditCardInfo;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.ANY,setterVisibility=JsonAutoDetect.Visibility.ANY)
public class CreditCardValidatorServiceResponse  implements Serializable  {

	private String serviceTransactionId;
	private boolean isValid;
	private CreditCardInfo cardInfo;
	
	
	public boolean isValid() {
		return isValid;
	}
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}
	public CreditCardInfo getCardInfo() {
		return cardInfo;
	}
	public void setCardInfo(CreditCardInfo cardInfo) {
		this.cardInfo = cardInfo;
	}
	public String getServiceTransactionId() {
		return serviceTransactionId;
	}
	public void setServiceTransactionId(String serviceTransactionId) {
		this.serviceTransactionId = serviceTransactionId;
	}
	
	
}
