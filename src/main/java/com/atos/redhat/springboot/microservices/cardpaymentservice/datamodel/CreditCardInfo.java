package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.ANY,setterVisibility=JsonAutoDetect.Visibility.ANY)
public class CreditCardInfo implements Serializable {

	private Long cardNo;
	private int securityCode;
	private Date expriryDate;
	
	public Long getCardNo() {
		return cardNo;
	}
	public void setCardNo(Long cardNo) {
		this.cardNo = cardNo;
	}
	public int getSecurityCode() {
		return securityCode;
	}
	public void setSecurityCode(int securityCode) {
		this.securityCode = securityCode;
	}
	public Date getExpriryDate() {
		return expriryDate;
	}
	public void setExpriryDate(Date expriryDate) {
		this.expriryDate = expriryDate;
	}
	
	
}
