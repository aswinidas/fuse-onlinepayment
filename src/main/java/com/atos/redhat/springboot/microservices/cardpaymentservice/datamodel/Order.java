package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.ANY,setterVisibility=JsonAutoDetect.Visibility.ANY)
public class Order  implements Serializable  {
	
	private String orderId;
	private String orderType;
	private int orderQuantity;
	private String orderPrice;
	
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getOrderType() {
		return orderType;
	}
	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}
	public int getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(int orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	public String getOrderPrice() {
		return orderPrice;
	}
	public void setOrderPrice(String orderPrice) {
		this.orderPrice = orderPrice;
	}
	
	
}
