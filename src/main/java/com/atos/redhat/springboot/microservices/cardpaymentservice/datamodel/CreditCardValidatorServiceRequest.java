package com.atos.redhat.springboot.microservices.cardpaymentservice.datamodel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility=JsonAutoDetect.Visibility.ANY,setterVisibility=JsonAutoDetect.Visibility.ANY)
public class CreditCardValidatorServiceRequest implements Serializable  {

	private String requestId;
	private CreditCardInfo cardInfo;
	private CustomerInfo customerInfo;
	public String getRequestId() {
		return requestId;
	}
	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}
	public CreditCardInfo getCardInfo() {
		return cardInfo;
	}
	public void setCardInfo(CreditCardInfo cardInfo) {
		this.cardInfo = cardInfo;
	}
	public CustomerInfo getCustomerInfo() {
		return customerInfo;
	}
	public void setCustomerInfo(CustomerInfo customerInfo) {
		this.customerInfo = customerInfo;
	}
	
	
	
}
